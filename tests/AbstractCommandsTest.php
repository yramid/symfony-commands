<?php

declare(strict_types=1);

namespace Yramid\Test;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Yramid\YramidInterface;

abstract class AbstractCommandsTest extends TestCase
{
    protected YramidInterface|MockObject $yramid;

    protected int $serial;

    protected string $migrationName;

    protected string $migrationNamespace;

    protected string $migrationPath;

    protected string $seedNamespace;

    protected string $seedPath;

    protected array $configuration;

    public function setUp(): void
    {
        parent::setUp();

        $this->yramid = $this->createMock(YramidInterface::class);

        $this->configuration = include 'config.php';

        $this->serial = (int) (new \DateTimeImmutable())->format('YmdHis');
        $this->migrationName = 'ThisIsADescriptionString';
        $this->migrationNamespace = 'Migration';
        $this->migrationPath = __DIR__ . '/../../db/migrations';
        $this->seedNamespace = 'Seeds';
        $this->seedPath = __DIR__ . '/../../db/seeds';
    }
}
