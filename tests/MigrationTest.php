<?php

declare(strict_types=1);

namespace Yramid\Test;

use Closure;
use RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;
use Yramid\ConfigAccessor;
use Yramid\Migration\Create;
use Yramid\Migration\Migrate;
use Yramid\Migration\MigrationData;
use Yramid\Migration\MigrationStatus;
use Yramid\Migration\Rollback;
use Yramid\Migration\Savepoint\Remove;
use Yramid\Migration\Savepoint\Set;
use Yramid\Migration\Status;

final class MigrationTest extends AbstractCommandsTest
{
    private function getMigrationData(): MigrationData
    {
        return new MigrationData(
            $this->serial,
            $this->migrationName,
            MigrationStatus::NEW,
            null,
            null,
            "$this->migrationPath/{$this->serial}_$this->migrationName.php",
            "$this->migrationNamespace\\$this->migrationName",
        );
    }

    public function testCreateMigration(): void
    {
        $tester = new CommandTester(
            new Create(
                'dbm:create',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migration = $this->getMigrationData();
        $this->yramid->expects($this->once())
            ->method('createMigration')
            ->with(
                $this->configuration,
                $this->serial,
                $this->migrationName,
            )
            ->willReturn($migration);

        $this->assertSame(
            0,
            $tester->execute(['description' => 'This is a description string']),
        );

        $migrationPath = ConfigAccessor::getMigrationTemplate($this->configuration);
        $expected = <<<CONSOLE
            using migration path 
            $migrationPath
            created {$this->serial}_$this->migrationName.php
        CONSOLE;

        $this->assertSame(
            $expected . PHP_EOL,
            $tester->getDisplay(),
        );
    }

    public function testException(): void
    {
        $tester = new CommandTester(
            new Migrate(
                'dbm:migrate',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->expectExceptionObject(
            new RuntimeException('Target is not a string'),
        );

        $tester->execute(
            [
                '--target' => 47.11,
                '--dry-run' => true,
            ],
        );
    }

    public function testMigrate(): void
    {
        $tester = new CommandTester(
            new Migrate(
                'dbm:migrate',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migrationCallback = function (Closure $closure): bool {
            $closure(
                $this->getMigrationData(),
                MigrationStatus::NEW,
                MigrationStatus::UP,
                .007
            );
            return true;
        };

        $this->yramid->expects($this->once())
            ->method('migrateUp')
            ->with(
                $this->configuration,
                $this->serial,
                true,
                $this->callback($migrationCallback),
            );

        $this->assertSame(
            0,
            $tester->execute(
                [
                    '--target' => (string) $this->serial,
                    '--dry-run' => true,
                ],
            ),
        );

        $expected = <<<CONSOLE
            using migration path 
            == $this->serial $this->migrationName: migrated 0.0070
            All Done.
            CONSOLE . PHP_EOL;

        $this->assertSame(
            $expected,
            $tester->getDisplay(),
        );
    }

    public function testMigrateWithDefaults(): void
    {
        $tester = new CommandTester(
            new Migrate(
                'dbm:migrate',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migrationData = new MigrationData(
            $this->serial,
            $this->migrationName,
            MigrationStatus::NEW,
            null,
            null,
            null,
            null,
        );

        $this->yramid->expects($this->once())
            ->method('getMigrationStatus')
            ->with($this->configuration)
            ->willReturn([$this->serial => $migrationData]);

        $migrationCallback = function (Closure $closure): bool {
            $closure(
                $this->getMigrationData(),
                MigrationStatus::NEW,
                MigrationStatus::UP,
                .007
            );
            return true;
        };

        $this->yramid->expects($this->once())
            ->method('migrateUp')
            ->with(
                $this->configuration,
                $this->serial,
                false,
                $this->callback($migrationCallback),
            );

        $this->assertSame(
            0,
            $tester->execute([]),
        );

        $expected = <<<CONSOLE
            using migration path 
            == $this->serial $this->migrationName: migrated 0.0070
            All Done.
            CONSOLE . PHP_EOL;

        $this->assertSame(
            $expected,
            $tester->getDisplay(),
        );
    }

    public function testMigrateWithNoMigrations(): void
    {
        $tester = new CommandTester(
            new Migrate(
                'dbm:migrate',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->assertSame(
            0,
            $tester->execute([]),
        );

        $this->assertSame(
            'No migrations to migrate found' . PHP_EOL,
            $tester->getDisplay(),
        );
    }

    public function testRollback(): void
    {
        $tester = new CommandTester(
            new Rollback(
                'dbm:rollback',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migrationCallback = function (Closure $closure): bool {
            $closure(
                $this->getMigrationData(),
                MigrationStatus::UP,
                MigrationStatus::DOWN,
                .42
            );
            return true;
        };

        $this->yramid->expects($this->once())
            ->method('migrateDown')
            ->with(
                $this->configuration,
                $this->serial,
                true,
                $this->callback($migrationCallback),
            );

        $this->assertSame(
            0,
            $tester->execute(
                [
                    '--target' => (string) $this->serial,
                    '--dry-run' => true,
                ],
            ),
        );

        $expected = <<<CONSOLE
            using migration path 
            == $this->serial $this->migrationName: reverted 0.4200
            All Done.
            CONSOLE . PHP_EOL;

        $this->assertSame(
            $expected,
            $tester->getDisplay(),
        );
    }

    public function testRollbackWithDefaults(): void
    {
        $tester = new CommandTester(
            new Rollback(
                'dbm:rollback',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migrationData = new MigrationData(
            $this->serial,
            $this->migrationName,
            MigrationStatus::UP,
            '2021-12-03T11:17:45+0100',
            null,
            null,
            null,
        );

        $this->yramid->expects($this->once())
            ->method('getMigrationStatus')
            ->with($this->configuration)
            ->willReturn([$this->serial => $migrationData]);

        $migrationCallback = function (Closure $closure): bool {
            $closure(
                $this->getMigrationData(),
                MigrationStatus::UP,
                MigrationStatus::DOWN,
                .42
            );
            return true;
        };

        $this->yramid->expects($this->once())
            ->method('migrateDown')
            ->with(
                $this->configuration,
                $this->serial,
                false,
                $this->callback($migrationCallback),
            );

        $this->assertSame(
            0,
            $tester->execute([]),
        );

        $expected = <<<CONSOLE
            using migration path 
            == $this->serial $this->migrationName: reverted 0.4200
            All Done.
            CONSOLE . PHP_EOL;

        $this->assertSame(
            $expected,
            $tester->getDisplay(),
        );
    }

    public function testRollbackWithNoMigrations(): void
    {
        $tester = new CommandTester(
            new Rollback(
                'dbm:rollback',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->assertSame(
            0,
            $tester->execute([]),
        );

        $this->assertSame(
            'No migrations for rollback found' . PHP_EOL,
            $tester->getDisplay(),
        );
    }

    public function testStatus(): void
    {
        $tester = new CommandTester(
            new Status(
                'dbm:status',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migrationData = new MigrationData(
            $this->serial,
            $this->migrationName,
            MigrationStatus::UP,
            '2021-12-03T11:17:45+0100',
            null,
            null,
            null,
        );

        $status = [
            1639044913 => new MigrationData(
                1639044913,
                'FirstMigration',
                MigrationStatus::DOWN,
                '2021-12-03T11:17:45+0100',
                null,
                null,
                null,
            ),
            1639044914 => new MigrationData(
                1639044914,
                'SecondMigration',
                MigrationStatus::UP,
                '2022-01-03T11:17:45+0100',
                'This is a savepoint',
                null,
                null,
            ),
            1639044915 => new MigrationData(
                1639044915,
                'ThirdMigration',
                MigrationStatus::MISSING,
                '2022-02-03T11:17:45+0100',
                null,
                null,
                null,
            ),
            1639044916 => new MigrationData(
                1639044916,
                'LastMigration',
                MigrationStatus::NEW,
                '2022-03-03T11:17:45+0100',
                null,
                null,
                null,
            ),
        ];

        $this->yramid->expects($this->once())
            ->method('getMigrationStatus')
            ->with($this->configuration)
            ->willReturn($status);

        $this->assertSame(
            0,
            $tester->execute([]),
        );

        $output = <<<CONSOLE
             --------- ------------ ----------------- -------------------------- --------------------- 
              Status    Serial       Name              Timestamp                  Savepoint            
             --------- ------------ ----------------- -------------------------- --------------------- 
              DOWN      1639044913   FirstMigration    2021-12-03T11:17:45+0100                        
              UP        1639044914   SecondMigration   2022-01-03T11:17:45+0100   This is a savepoint  
              MISSING   1639044915   ThirdMigration    2022-02-03T11:17:45+0100                        
              NEW       1639044916   LastMigration     2022-03-03T11:17:45+0100                        
             --------- ------------ ----------------- -------------------------- --------------------- 


            CONSOLE;

        $this->assertSame(
            $output,
            $tester->getDisplay(),
        );
    }

    public function testSetSavepoint(): void
    {
        $tester = new CommandTester(
            new Set(
                'dbm:set-savepoint',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->yramid->expects($this->once())
            ->method('setMigrationSavepoint')
            ->with(
                $this->configuration,
                $this->serial,
                'MySavePoint',
            );

        $this->assertSame(
            0,
            $tester->execute(
                [
                    'savepoint' => 'MySavePoint',
                    '--target' => (string) $this->serial,
                ],
            ),
        );
    }

    public function testSetSavepointWithStringAsTarget(): void
    {
        $tester = new CommandTester(
            new Set(
                'dbm:set-savepoint',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $target = 'MyTargetString';
        $savepoint = 'MySavePoint';

        $this->yramid->expects($this->once())
            ->method('setMigrationSavepoint')
            ->with(
                $this->configuration,
                $target,
                $savepoint,
            );

        $this->assertSame(
            0,
            $tester->execute(
                [
                    'savepoint' => $savepoint,
                    '--target' => $target,
                ],
            ),
        );
    }

    public function testSetSavepointWithDefaults(): void
    {
        $tester = new CommandTester(
            new Set(
                'dbm:set-savepoint',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $migrationData = new MigrationData(
            $this->serial,
            $this->migrationName,
            MigrationStatus::UP,
            null,
            null,
            null,
            null,
        );

        $this->yramid->expects($this->once())
            ->method('getMigrationStatus')
            ->with($this->configuration)
            ->willReturn([$this->serial => $migrationData]);

        $this->yramid->expects($this->once())
            ->method('setMigrationSavepoint')
            ->with(
                $this->configuration,
                $this->serial,
                'MySavePoint',
            );

        $this->assertSame(
            0,
            $tester->execute(['savepoint' => 'MySavePoint']),
        );
    }

    public function testSetSavepointWithNoMigrations(): void
    {
        $tester = new CommandTester(
            new Set(
                'dbm:set-savepoint',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->assertSame(
            0,
            $tester->execute(['savepoint' => 'MySavePoint']),
        );

        $this->assertSame(
            'No migrations available to set savepoint given' . PHP_EOL,
            $tester->getDisplay(),
        );
    }

    public function testRemoveSavepoint(): void
    {
        $tester = new CommandTester(
            new Remove(
                'dbm:unset-savepoint',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->yramid->expects($this->once())
            ->method('setMigrationSavepoint')
            ->with(
                $this->configuration,
                'MySavePoint',
                null,
            );

        $this->assertSame(
            0,
            $tester->execute(['savepoint' => 'MySavePoint']),
        );
    }
}
