<?php

declare(strict_types=1);

namespace Yramid\Test;

use Symfony\Component\Console\Tester\CommandTester;
use Yramid\Seed\Create;
use Yramid\Seed\Run;
use Yramid\Seed\SeedData;

final class SeedTest extends AbstractCommandsTest
{
    protected string $seedName;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedName = 'ThisIsADescriptionString';
    }

    public function testCreate(): void
    {
        $tester = new CommandTester(
            new Create(
                'seed:create',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $seedData = new SeedData(
            $this->seedName,
            $this->seedPath . "/$this->seedName.php",
            "$this->seedNamespace\\$this->seedName",
        );

        $this->yramid->expects($this->once())
            ->method('createSeed')
            ->with(
                $this->configuration,
                $this->seedName,
            )
            ->willReturn($seedData);

        $this->assertSame(
            0,
            $tester->execute(['description' => 'This is a description string']),
        );

        $this->assertSame(
            "$this->seedName.php was created" . PHP_EOL,
            $tester->getDisplay(),
        );
    }

    public function testRun(): void
    {
        $tester = new CommandTester(
            new Run(
                'seed:run',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->yramid->expects($this->once())
            ->method('runSeed')
            ->with(
                $this->configuration,
                $this->seedName,
                true,
            );

        $this->assertSame(
            0,
            $tester->execute(
                [
                    'name' => $this->seedName,
                    '--dry-run' => true,
                ],
            ),
        );

        $expected = <<<CONSOLE
            using seed path 
            == $this->seedName
            Done after runtime 0.000
            CONSOLE;

        $this->assertTrue(
            str_starts_with($tester->getDisplay(), $expected),
        );
    }

    public function testRunWithDefaults(): void
    {
        $tester = new CommandTester(
            new Run(
                'seed:run',
                __DIR__ . '/../tests/config.php',
                $this->yramid,
            ),
        );

        $this->yramid->expects($this->once())
            ->method('runSeed')
            ->with(
                $this->configuration,
                $this->seedName,
                false,
            );

        $this->assertSame(
            0,
            $tester->execute(['name' => $this->seedName]),
        );
    }
}
