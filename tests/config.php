<?php

return [
    'migrations' => [
        'path' => __DIR__ . '/../../db/migrations',
        'namespace' => 'Migration',
        'template' => null,
    ],
    'seeds' => [
        'path' => __DIR__ . '/../../db/seeds',
        'namespace' => 'Seeds',
        'template' => null,
    ],
    'connection' => 'sqlite::memory:',
];
