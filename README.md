# Documentation

## Setup

### 1. Create configuration

Create an accessible PHP file for the script with appropriate configuration:

```php
<?php

return [
    'migrations' => [
        'path' => __DIR__ . '/../../db/migrations',
        'namespace' => 'Migration',
        'template' => null,
    ],
    'seeds' => [
        'path' => __DIR__ . '/../../db/seeds',
        'namespace' => 'Seeds',
        'template' => null,
    ],
    'connection' => 'sqlite:' . __DIR__ . '/../../db/migration.sqlite',
];
```

_In the example relative to the project ROOT dev/db/config/migration.php_.

`connection` can be both a __DSN__ and a PDO instance.
(https://www.php.net/manual/de/pdo.construct.php)

### 2. Create CLI script

Depending on the configuration file, an executable CLI script can now be created in which the
Command classes are defined with corresponding constructor parameters.

```php
#!/usr/bin/env php
<?php

use Symfony\Component\Console\Application;
use Yramid\Migration\Create;
use Yramid\Seed\Create as CreateSeed;
use Yramid\Migration\Migrate;
use Yramid\Migration\Rollback;
use Yramid\Seed\Run;
use Yramid\Migration\Savepoint\Set;
use Yramid\Migration\Status;
use Yramid\Migration\Savepoint\Remove;
use Yramid\Yramid;

require_once __DIR__ . '/../../vendor/autoload.php';

try {
    $migrationConfig = __DIR__ . '/../db/config/migration.php';

    $yramid = new Yramid();
    $app = new Application();
    $app->addCommands([
        (new Create('dbm:create', $migrationConfig, $yramid)),
        (new Status('dbm:status', $migrationConfig, $yramid)),
        (new Migrate('dbm:migrate', $migrationConfig, $yramid)),
        (new Rollback('dbm:rollback', $migrationConfig, $yramid)),
        (new CreateSeed('seed:create', $migrationConfig, $yramid)),
        (new Run('seed:run', $migrationConfig, $yramid)),
        (new Set('dbm:set-savepoint', $migrationConfig, $yramid)),
        (new Remove('dbm:unset-savepoint', $migrationConfig, $yramid)),
    ]);
    $app->run();
} catch (Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
}
```

_In the example relative to the project ROOT `dev/bin/cli`_.

## Usage

Depending on the __definitions__ in the CLI script, the commands are now accessible.

```
 dbm
  dbm:create           Use to create new migration files
  dbm:migrate          Migrate migration
  dbm:rollback         Rollback migration
  dbm:set-savepoint    Set migration savepoint
  dbm:status           List migration status
  dbm:unset-savepoint  Unset migration savepoint
 seed
  seed:create          Create a seed
  seed:run             Run seed
```

### CreateCommand

To create new migration scripts.

```shell

cli dbm:create <description>
```

| Argument     | Description                                                                        |
|--------------|------------------------------------------------------------------------------------|
| description  | The migration description. From this the `name` and valid class name is generated. |

### StatusCommand

To output the status of all migrations.

```shell

cli dbm:status
```

```text
 -------- ------------ ------------------------ -------------------------- -----------
  Status   Serial       Name                     Timestamp                  Savepoint  
 -------- ------------ ------------------------ -------------------------- -----------
  UP       1638271429   PartOne                  2021-12-03T11:17:45+0100              
  UP       1638271911   PartTwo                  2021-12-03T11:17:45+0100              
  UP       1638347995   SetAllTimestampFormats   2021-12-03T11:21:13+0100              
  UP       1638462559   DefineNewRelation        2021-12-03T11:21:13+0100   Save-rel0
  DOWN     1638462617   YourCurrentMigration     
 -------- ------------ ------------------------ -------------------------- -----------
```
_Exemplary status output_

### MigrateCommand

To perform migrations to a specified destination `target`.

```shell

cli dbm:migrate [options]
```

| Option                | Description                                                                   |
|-----------------------|-------------------------------------------------------------------------------|
| -t, --target[=TARGET] | Target `serial` or `savepoint` up to which you want to migrate;               |
| -d, --dry-run         | Execution in `dry-run` mode. (No real migration but DB transaction rollback). |

### RollbackCommand

Rolls back migrations to a specified `target`.

```shell

cli dbm:rollback [options]
```

| Option                | Description                                                                            |
|-----------------------|----------------------------------------------------------------------------------------|
| -t, --target[=TARGET] | Target `serial` or `savepoint` to be rolled back to;                                   |
| -d, --dry-run         | Execution in `dry-run` mode. (No real migration-rollback but DB transaction rollback). |

### CreateSeedCommand

For creation of seeds.

```shell

cli  seed:create <description>
```

| Argument     | Description                                                                   |
|--------------|-------------------------------------------------------------------------------|
| description  | The seed description. From this the `name` and valid class name is generated. |

### RunSeedCommand

For processing specific seeds.

```shell

cli seed:run [options] [--] <name>
```

| Argument             | Description                        |
|----------------------|------------------------------------|
| name                 | Name of the seed to be processed.  |

| Option               | Beschreibung                                                                  |
|----------------------|-------------------------------------------------------------------------------|
| -d, --dry-run        | Execution in `dry-run` mode. (No real DB commit but DB transaction rollback). |

### SetSavepointCommand

For the definition of savepoints.

```shell

cli dbm:set-savepoint [options]
```
| Argument          | Description                       |
|-------------------|-----------------------------------|
| savepoint         | String of the `savepoint` to set; |

| Option                | Description                                                         |
|-----------------------|---------------------------------------------------------------------|
| -t, --target[=TARGET] | Target <serial> of migration at this the `savepoint` should be set. |

### RemoveSavepointCommand

To remove a savepoint from a migration.

```shell

cli dbm:unset-savepoint [options]
```

| Argument          | Description                                     |
|-------------------|-------------------------------------------------|
| savepoint         | String of the `savepoint` or `serial` to unset; |
