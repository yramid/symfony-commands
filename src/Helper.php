<?php

declare(strict_types=1);

namespace Yramid;

class Helper
{
    /**
     * @param string $name
     *
     * @return non-empty-string
     */
    public static function sanitizeClassName(string $name): string
    {
        return preg_replace_callback(
            '{(?:\W+|^)(\w)}',
            static fn (array $match): string => strtoupper($match[1]),
            $name,
        );
    }
}
