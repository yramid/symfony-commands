<?php

declare(strict_types=1);

namespace Yramid;

use RuntimeException;
use Symfony\Component\Console\Command\Command;

abstract class AbstractCommand extends Command
{
    public function __construct(
        string $name,
        private string $configFile,
        protected YramidInterface $yramid,
    ) {
        parent::__construct($name);
    }

    protected function setUp(): array
    {
        $configuration = include $this->configFile;
        $this->yramid->setUp($configuration);

        return $configuration;
    }

    protected function getTargetOption(mixed $target): null|int|string
    {
        if ($target === null) {
            return null;
        }

        if (!is_string($target)) {
            throw new RuntimeException('Target is not a string');
        }

        if (is_numeric($target)) {
            return intval($target);
        }

        return $target;
    }
}
