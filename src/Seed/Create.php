<?php

declare(strict_types=1);

namespace Yramid\Seed;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yramid\AbstractCommand;
use Yramid\Helper;

final class Create extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();

        $this->setDescription('Create a new database seed')
            ->addArgument(
                'description',
                InputArgument::REQUIRED,
                'The required seed description will also used for class name generation in camel case.',
            );

        $helpText = <<<TEXT
            From the <fg=cyan><description></> argument the seed name is generated. This also corresponds to the valid class name of the seed.
            Example: "Yet another seed file" generates a <fg=cyan><serial>_YetAnotherSeedFile.php</> migration file.
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = Helper::sanitizeClassName(
            $input->getArgument('description'),
        );

        $config = $this->setUp();
        $data = $this->yramid->createSeed(
            $config,
            $name,
        );

        $output->writeln('<info>' . basename($data->fileName) . ' was created</info>');
        return 0;
    }
}
