<?php

declare(strict_types=1);

namespace Yramid\Seed;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Yramid\AbstractCommand;
use Yramid\ConfigAccessor;

final class Run extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();

        $this->setDescription('Run a database seed')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'The seed name to run'
            )->addOption(
                'dry-run',
                'd',
                InputOption::VALUE_NONE,
                'Executing in <fg=green>dry-run</> mode; (boolean, default=false)',
            );

        $helpText = <<<TEXT
            Run the seed defined by the <fg=cyan><name></> (string) argument.
            ---
            As soon as the option <fg=cyan><dry-run></> (boolean) is set, the database transaction of the seeds is not committed, but a rollback is executed. This is not the case by default.
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var non-empty-string $name */
        $name = $input->getArgument('name');
        $config = $this->setUp();

        $seedPath = realpath(ConfigAccessor::getSeedPath($config));
        $output->writeln("<info>using seed path $seedPath</info>\n== <info>{$name}</info>");

        $timeStart = microtime(true);
        $this->yramid->runSeed(
            $config,
            $name,
            $input->getOption('dry-run'),
        );

        $runtime = number_format(
            microtime(true) - $timeStart,
            4,
        );

        $output->writeln("<comment>Done after runtime $runtime</comment>");

        return 0;
    }
}
