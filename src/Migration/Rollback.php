<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Yramid\AbstractCommand;
use Yramid\ConfigAccessor;

final class Rollback extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();

        $this->setDescription(
            'The rollback command reverts the last migration, or optionally down to a specific serial or savepoint as target option <fg=green>(-t, --target[=TARGET])</>',
        )->addOption(
            'target',
            't',
            InputOption::VALUE_OPTIONAL,
            'Target <fg=magenta>serial</> (integer of 10 digests) or <fg=magenta>savepoint</> (free customizable string)',
        )->addOption(
            'dry-run',
            'd',
            InputOption::VALUE_NONE,
            'Executing in <fg=green>dry-run</> mode; (boolean, default=false)',
        );

        $helpText = <<<TEXT
            A <fg=cyan><target></> option can be either a valid <fg=magenta>"serial"</> (integer) or a <fg=magenta>"savepoint"</> (quoted string) up to which to rollback.
            If no <fg=cyan><target></> option is specified, the last possible migration is considered the <fg=cyan><target></> by default.
            ---
            As soon as the option <fg=cyan><dry-run></> (boolean) is set, the database transaction of the migrations is not committed, but a rollback is executed. This is not the case by default.
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configuration = $this->setUp();

        /** @var null|non-empty-string|positive-int $target */
        $target
            = $input->getOption('target')
            ?? $this->getLastUpSerial($configuration);

        if (!$target) {
            $output->writeln('<info>No migrations for rollback found</info>');
            return 0;
        }

        $callback = function (
            MigrationData $result,
            string $migrationStatus,
            string $migrationStatusNew,
            float $runtime,
        ) use ($output): void {
            if ($migrationStatus !== $migrationStatusNew) {
                $runtime = number_format($runtime, 4);
                $output->writeln("== <info>$result->serial $result->name:</info> <comment>reverted $runtime</comment>");
            }
        };

        $output->writeln(
            sprintf(
                "<info>using migration path %s</info>",
                realpath(ConfigAccessor::getMigrationPath($configuration)),
            ),
        );

        /** @var positive-int|non-empty-string $target */
        $this->yramid->migrateDown(
            $configuration,
            $target,
            $input->getOption('dry-run'),
            $callback,
        );

        $output->writeln('All Done.');

        return 0;
    }

    /**
     * @param array $configuration
     *
     * @return null|positive-int
     */
    private function getLastUpSerial(array $configuration): ?int
    {
        $filteredStatus = array_filter(
            $this->yramid->getMigrationStatus($configuration),
            fn(MigrationData $migrationData): bool => $migrationData->status === MigrationStatus::UP,
        );

        $target = array_pop($filteredStatus);

        return $target?->serial;
    }
}
