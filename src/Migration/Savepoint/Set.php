<?php

declare(strict_types=1);

namespace Yramid\Migration\Savepoint;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Yramid\AbstractCommand;
use Yramid\Migration\MigrationData;
use Yramid\Migration\MigrationStatus;

final class Set extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();

        $this->setDescription('Set a custom savepoint to a migration')
            ->addArgument(
                'savepoint',
                InputArgument::REQUIRED,
                'The savepoint is a free customizable string',
            )->addOption(
                'target',
                't',
                InputOption::VALUE_OPTIONAL,
                'Target <fg=magenta>serial</> (integer of 10 digests) or <fg=magenta>savepoint</> (free customizable string)',
            );

        $helpText = <<<TEXT
            The required quoted <fg=cyan><savepoint></> argument is a free customizable string.
            A <fg=cyan><target></> option can be either a valid <fg=magenta>"serial"</> (integer) or a <fg=magenta>"savepoint"</> (quoted string) up to which to define the <fg=cyan><savepoint></> value.
            If no <fg=cyan><target></> option is specified, the last possible migration is considered the <fg=cyan><target></> by default.
            By the <fg=yellow>migration status command</> a list of all migrations and there optional <fg=magenta>"savepoint"</> are shown. 
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configuration = $this->setUp();

        $target = $this->getTargetOption(
            $input->getOption('target'),
        );

        if ($target === null) {
            $target = $this->getLastNewSerial($configuration);
        }

        if ($target === null) {
            $output->writeln('<info>No migrations available to set savepoint given</info>');
            return 0;
        }

        /** @var non-empty-string $savepoint */
        $savepoint = $input->getArgument('savepoint');

        /** @var non-empty-string|positive-int $target */
        $this->yramid->setMigrationSavepoint(
            $configuration,
            $target,
            $savepoint,
        );

        $output->writeln("<info>Savepoint <comment>$savepoint</comment> was set on target <comment>$target</comment></info>");

        return 0;
    }

    private function getLastNewSerial(array $configuration): ?int
    {
        $filteredStatus = array_filter(
            $this->yramid->getMigrationStatus($configuration),
            fn(MigrationData $migrationData): bool => $migrationData->status !== MigrationStatus::NEW,
        );

        $target = array_pop($filteredStatus);

        return $target?->serial;
    }
}
