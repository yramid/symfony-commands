<?php

declare(strict_types=1);

namespace Yramid\Migration\Savepoint;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yramid\AbstractCommand;

final class Remove extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();

        $this->setDescription('Unset a custom savepoint of a migration')
            ->addArgument(
                'savepoint',
                InputArgument::REQUIRED,
                'The savepoint is a free customizable string',
            );

        $helpText = <<<TEXT
            To remove a savepoint from a migration use this command with the required quoted <fg=cyan><savepoint></> argument.
            It can be either a valid <fg=magenta>"serial"</> (integer) or a <fg=magenta>"savepoint"</> (quoted string).
            By the <fg=yellow>migration status command</> a list of all migrations and there optional <fg=magenta>"savepoint"</> are shown. 
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configuration = $this->setUp();

        /** @var non-empty-string $savepoint */
        $savepoint = $input->getArgument('savepoint');

        $this->yramid->setMigrationSavepoint(
            $configuration,
            $savepoint,
            null,
        );

        $output->writeln("<info>Savepoint <comment>$savepoint</comment> was removed.</info>");

        return 0;
    }
}
