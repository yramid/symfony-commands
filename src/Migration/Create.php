<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yramid\AbstractCommand;
use Yramid\ConfigAccessor;
use Yramid\Helper;

final class Create extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();

        $this->setDescription('Create a new migration')
            ->addArgument(
                'description',
                InputArgument::REQUIRED,
                'The required migration description will also used for class name generation in camel case.',
            );

        $helpText = <<<TEXT
            From the <fg=cyan><description></> argument the migration name is generated. This also corresponds to the valid class name of the migration.
            Example: "Yet another migration part 1" generates a <fg=cyan><serial>_YetAnotherMigrationPart1.php</> migration file.
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $name = Helper::sanitizeClassName(
            $input->getArgument('description'),
        );

        $configuration = $this->setUp();
        $migration = $this->yramid->createMigration(
            $configuration,
            (int) (new \DateTimeImmutable())->format('YmdHis'),
            $name,
        );

        $tmpl = <<<CONSOLE
            <info>using migration path %s
            %s
            created</info> %s
        CONSOLE;

        $output->writeln(
            sprintf(
                $tmpl,
                realpath(ConfigAccessor::getMigrationPath($configuration)),
                ConfigAccessor::getMigrationTemplate($configuration),
                basename($migration->fileName),
            ),
        );

        return 0;
    }
}
