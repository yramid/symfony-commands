<?php

declare(strict_types=1);

namespace Yramid\Migration;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Yramid\AbstractCommand;

final class Status extends AbstractCommand
{
    protected function configure(): void
    {
        parent::configure();
        $this->setDescription('List of all migrations with status and further information');

        $helpText = <<<TEXT
            By this <fg=yellow>command</> a list of all migrations with additional information are shown. 
            TEXT;

        $this->setHelp($helpText);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $config = $this->setUp();

        $rows = [];
        foreach ($this->yramid->getMigrationStatus($config) as $migrationData) {
            $rows[] = [
                $this->getLabelByStatus($migrationData->status),
                $migrationData->serial,
                $migrationData->name,
                $migrationData->timestamp,
                $migrationData->savepoint,
            ];
        }

        if ($rows) {
            (new SymfonyStyle($input, $output))->table(
                ['Status', 'Serial', 'Name', 'Timestamp', 'Savepoint'],
                $rows,
            );
        }

        return 0;
    }

    private function getLabelByStatus(string $status): string
    {
        return match ($status) {
            MigrationStatus::DOWN => '<comment>DOWN</comment>',
            MigrationStatus::MISSING => '<error>MISSING</error>',
            MigrationStatus::NEW => '<comment>NEW</comment>',
            MigrationStatus::UP => '<info>UP</info>',
        };
    }
}
